# Copyright 2020 Johannes Nixdorf <mixi@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

SUMMARY="A set of network management utilities"
DESCRIPTION="
bcnm is a client network manager: it automatically handles network connections
for a client machine. It supports Ethernet and Wi-Fi. IP addresses can be
attributed statically or via DHCP.

This is very much a work in progress. Do not expect full functionality in
the short or even middle term.
"
HOMEPAGE="http://skarnet.org/software/${PN}/"
DOWNLOADS="http://skarnet.org/software/${PN}/${PNV}.tar.gz"

LICENCES="ISC"
SLOT="0"
PLATFORMS="~amd64 ~armv8"
MYOPTIONS="
    static
"

DEPENDENCIES="
    build+run:
        dev-libs/skalibs[>=2.14.3.0]
"

BUGS_TO="mixi@exherbo.org"

DEFAULT_SRC_COMPILE_PARAMS=(
    CROSS_COMPILE=$(exhost --tool-prefix)
)

src_configure()
{
    local args=(
        --enable-shared
        --with-lib=/usr/$(exhost --target)/lib
        --with-dynlib=/usr/$(exhost --target)/lib
        $(option_enable static allstatic)
    )

    [[ $(exhost --target) == *-gnu* ]] || \
        args+=( $(option_enable static static-libc) )

    econf "${args[@]}"
}

src_install()
{
    default

    insinto /usr/share/doc/${PNVR}/html
    doins doc/*
}

